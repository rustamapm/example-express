FROM node:10-alpine
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN npm install
CMD [ "./run.sh" ]
